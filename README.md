# Usage Guide

- Chat system Front End driver operatoes on socket.io, Thus we need socket.io front end driver in our browser page.

    To embed socket.io script:
```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/3.0.1/socket.io.js"></script>
```

    **Note** don't use ES6 syntax on socket.io, as socket.io script has not exports

- Include FrontEnd driver scipt in JS code:

    **Copy** frontend driver's source code from here [frontend_driver/src/index.js](https://bitbucket.org/rishavbhowmiktgs/chat_system_front_end_driver/raw/63430a54d9e19bad2f2d5f6d7448611cc5356bc4/src/index.js)

```html
<script scr="<path>/frontend_driver/src/index.js"></script>
<script>
    //your code here
    //use classes - ChatLines, ChatSystem, FileStore
</script>
```

```html
//recommended 
<script type="module">
    import {ChatLines, ChatSystem, FileStore} from "<path>/frontend_driver/src/index.js"
    //your code here
</script>
```

## Usage

### Use the ChatSystem we need ChatSystem class
```js
//create an object of ChatSystem class
const chat = new ChatSystem(io/*io derives from socket.io js library*/)
```

### Connect to the backend and perform autorization
```js
chat.connect_and_authorize(user_token)
.then(when_connected)
.catch(when_not_connected)

function when_connected(){
    //you can now use ChatSystem class object to send and recive messages
}

function when_not_connected(){
    //tell user something went worng, maybe JWT token expired
}
```

### Creating a message use ChatLines class
```js
const message = new ChatLines('<reciver_s_user_id>')

//to added text
message.add_line_text(/*some text as string here*/)

//to let user upload a file
await message.add_line_file_auto_user_input_sync()
//or
message.add_line_file_auto_user_input_sync()
.then(()=>{/*upload done*/})
/*
add_line_file_auto_user_input_sync will prompt the user to choose a file from the device and upload it to File Store System
Once the upload is complete, or no file is choosen by the user, this function resolves the Promise object returned by it.
*/

//..............................................
//add line as blob will be added in next update
//..............................................
```

### Sending a message

```js
const response = await chat.send_msg(message/*must be instance of ChatLines class*/)
/*
send_msg return promise object
Once the server responds to send messesge request, the promise is resolved with id of the message
*/
```

### Reciving message

- Recive Message with `get_messages_bothway_post_id` function:
> This function retirive all messages of the user after a spcified _id in the argument

```js
const response_messages = await chat.get_messages_bothway_post_id(
        id_onwards/*<Number><Unsigned Integer>*/
    )

/*
    The resolve of returned Promise, returns an array of message objects
*/
```

- Recive Message with `get_messages_oneway_pleanty_of_sender` function:
> This function retirive all latest messages recived by user, present in DB of a sender

```js
const response_messages = await chat.get_messages_oneway_pleanty_of_sender(
        sender_id/*<string><MongodbObject Id>*/
    )

/*
    The resolve of returned Promise, returns an array of message objects
*/
```

- Recive Message with `get_messages_bothway_pleanty_of_sender` function:
> This function retirive all latest messages present in DB of a specific peer user

```js
const response_messages = await chat.get_messages_oneway_pleanty_of_sender(
        sender_id/*<string><MongodbObject Id>*/
    )

/*
    The resolve of returned Promise, returns an array of message objects
*/
```